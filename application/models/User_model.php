<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model{
    private $table;
    public function __construct(){
        parent::__construct();
        $this->table = 'user';
    }
    public function listUser(){
        $this->db->select('id, fullname, email, phone, age, address, birthday, gender');
        $this->db->from($this->table);
        $result = $this->db->get()->result_array();
        $this->db->flush_cache();
        return $result;
    }
    public function create($data = ''){
        $this->db->insert($this->table, $data);
        $result = $this->db->insert_id();
        $this->db->flush_cache();
        return $result;
    }
    public function update($data = '', $id = 0){
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        $result = $this->db->affected_rows(); //Số dòng thay đổi trong database
        $this->db->flush_cache();
        return $result;
    }
    public function delete($id = 0){
        $this->db->where('id', $id);
        $this->db->delete($this->table);
        $result = $this->db->affected_rows(); //Số dòng thay đổi trong database
        $this->db->flush_cache();
        return $result;
    }
    public function showByField($field = '', $value = ''){
        $this->db->select('id, fullname, email, password, salt, phone, age, address, birthday, gender');
        $this->db->from($this->table);
        $this->db->where(array(
            $field => $value,
        ));
        $result = $this->db->get()->row_array();
        $this->db->flush_cache();
        return $result;
    }
}
