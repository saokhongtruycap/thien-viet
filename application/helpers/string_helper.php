<?php
    if (!function_exists('random')){
        function random($length = 168, $char = FALSE){
            if ($char == FALSE) $s = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()';
            else $s = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            mt_srand((double)microtime() * 1000000);
            $salt = '';
            for ($i=0; $i<$length; $i++){
                $salt = $salt . substr($s, (mt_rand()%(strlen($s))), 1);
            }
            return $salt;
        }
    }

    if (!function_exists('password_encode')){
        function password_encode($password = '', $salt = ''){
            $password = md5(md5($password.$salt));
            return $password;
        }
    }
