<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller{
    private $checkLogin;
    public function __construct(){
        parent::__construct();
        $this->load->model('User_model');
        $this->load->helper('string');

        $this->checkLogin = (isset($_SESSION['authentication']) && !empty($_SESSION['authentication'])) ? $_SESSION['authentication'] : '';
        if ($this->checkLogin == ''){
            redirect(BASE_URL.'admin');
        }
    }
    public function index(){
        $listUser = $this->User_model->listUser();
        $data['listUser'] = $listUser;
        $this->load->view('user/index', (isset($data)) ? $data : '');
    }
    public  function create(){
        if ($this->input->post('create')){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('fullname', 'Họ tên', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback__checkEmail');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[10]|is_numeric|callback__checkPhone');
            $this->form_validation->set_rules('address', 'Address', 'trim|required');
            $this->form_validation->set_rules('age', 'Age', 'trim|required');
            $this->form_validation->set_rules('birthday', 'Birthday', 'trim|required');
            $this->form_validation->set_rules('gender', 'Gender');
            if ($this->form_validation->run($this)){
                $salt = random();
                $password = password_encode($this->input->post('password'), $salt);
                $data = array(
                    'fullname' => $this->input->post('fullname'),
                    'email' => $this->input->post('email'),
                    'password' => $password,
                    'salt' => $salt,
                    'phone' => $this->input->post('phone'),
                    'address' => $this->input->post('address'),
                    'age' => $this->input->post('age'),
                    'birthday' => $this->input->post('birthday'),
                    'gender' => $this->input->post('gender'),
                    'create_at' => gmdate('Y-m-d H:i:s', time() + 7*3600)
                );
                $result = $this->User_model->create($data);
                if ($result > 0){
                    $this->session->set_flashdata('message-success', 'Thêm mới thành công');
                    redirect(BASE_URL.'user/index');
                }
            }
        }
        $this->load->view('user/create');
    }

    public function update($id = 0){
        $id = (int)$id;
        $user = $this->User_model->showByField('id', $id);
        if ($this->input->post('update')){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('fullname', 'Họ tên', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback__checkEmail');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[10]|is_numeric|callback__checkPhone');
            $this->form_validation->set_rules('address', 'Address', 'trim|required');
            $this->form_validation->set_rules('age', 'Age', 'trim|required');
            $this->form_validation->set_rules('birthday', 'Birthday', 'trim|required');
            $this->form_validation->set_rules('gender', 'Gender');
            if ($this->form_validation->run($this)){
                $data = array(
                    'fullname' => $this->input->post('fullname'),
                    'email' => $this->input->post('email'),
                    'phone' => $this->input->post('phone'),
                    'address' => $this->input->post('address'),
                    'age' => $this->input->post('age'),
                    'birthday' => $this->input->post('birthday'),
                    'gender' => $this->input->post('gender'),
                    'update_at' => gmdate('Y-m-d H:i:s', time() + 7*3600)
                );
                $flag = $this->User_model->update($data, $id);
                if ($flag > 0){
                    $this->session->set_flashdata('message-success', 'Cập nhật dữ liệu thành công');
                    redirect(BASE_URL.'user/index');
                }

            }
        }
        $data['user'] = $user;
        $this->load->view('user/update', (isset($data)) ? $data : '');
    }
    public function delete($id = 0){
        $id = (int)$id;
        if ($this->input->post('delete')){
            $flag = $this->User_model->delete($this->input->post('id'));
            if ($flag > 0){
                $this->session->set_flashdata('message-success', 'Xóa dữ liệu thành công');
                redirect(BASE_URL.'user/index');
            }

        }
        $user = $this->User_model->showByField('id', $id);
        if (!isset($user) || !is_array($user) || count($user) == 0){
            $this->session->set_flashdata('message-error', 'Thành viên không tồn tại');
            redirect(BASE_URL.'user/index');
        }
        $data['user'] = $user;
        $this->load->view('user/delete', (isset($data)) ? $data : '');
    }
    public function _checkEmail($email = ''){
        $data = $this->User_model->showByField('email', $email);
        $emailOriginal = $this->input->post('email_original');
        if ($emailOriginal != $email){
            if (isset($data) && is_array($data) && count($data)){
                $this->form_validation->set_message('_checkEmail', 'Email đã tồn tại');
                return false;
            }
        }
        return true;
    }

    public function _checkPhone($phone = ''){
        $data = $this->User_model->showByField('phone', $phone);
        $phoneOriginal = $this->input->post('phone_original');
        if ($phoneOriginal != $phone){
            if (isset($data) && is_array($data) && count($data)){
                $this->form_validation->set_message('_checkPhone', 'Số điện thoại đã tồn tại');
                return false;
            }
        }
        return true;
    }
}