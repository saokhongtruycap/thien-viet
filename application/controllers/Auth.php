<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('User_model');
        $this->load->helper('string');

    }
    public function login(){
        if ($this->input->post('login')){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|callback__checkAuthentication');
            if ($this->form_validation->run($this)){
                $email = $this->input->post('email');
                $user = $this->User_model->showByField('email', $email);
                $_SESSION['authentication'] = json_encode(array(
                    'id' => $user['id'],
                    'fullname' => $user['fullname'],
                ));
                if ($user > 0){
                    $this->session->set_flashdata('message-success', 'Đăng nhập thành công');
                    redirect(BASE_URL.'user/index');
                }
            }
        }

        $this->load->view('auth/login');
    }
    public function _checkAuthentication(){
        $email = $this->input->post('email');
        $user = $this->User_model->showByField('email', $email);
        if (!isset($user) || !is_array($user) || count($user) == 0){
            $this->form_validation->set_message('_checkAuthentication', 'Tài khoản không tồn tại');
            return false;
        }
        $password = $this->input->post('password');
        $password = password_encode($password, $user['salt']);
        if ($user['password'] != $password){
            $this->form_validation->set_message('_checkAuthentication', 'Mật khẩu không chính xác');
            return false;
        }
        return true;
    }
}