<!DOCTYPE html>
<html>
<head>
    <link href="../../../resources/uikit/css/uikit.css" rel="stylesheet" />
    <link href="../../../resources/plugin.css" rel="stylesheet" />
    <link href="../../../resources/style.css" rel="stylesheet" />
    <base href="<?php echo BASE_URL;?>"
</head>
<body>
    <div class="login">
        <div class="uk-container uk-container-center">
                <div class="form">
                    <h2>Đăng nhập vào hệ thống</h2>
                    <?php $error = validation_errors(); echo (isset($error) && !empty($error)) ? $error : ''?>
                    <form action="" method="post">
                        <div>
                            <input type="text" value="<?php echo set_value('email')?>" name="email" placeholder="Email"/>
                        </div>
                        <div>
                            <input type="password" name="password" placeholder="Password"/>
                        </div>
                        <div>
                            <input type="submit" value="Login" name="login">
                        </div>
                        <div>
                            <a href="user/create">Register</a>
                            <a href="">Forgot Password?</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>