<!DOCTYPE html>
<html>
<head>
    <link href="../../../resources/uikit/css/uikit.css" rel="stylesheet" />
    <link href="../../../resources/plugin.css" rel="stylesheet" />
    <link href="../../../resources/style.css" rel="stylesheet" />
    <base href="<?php echo BASE_URL;?>"
</head>
<body>
<div class="index">
    <div class="uk-container uk-container-center">
        <h1>Danh sách User</h1>
        <table class="uk-table uk-table-striped">
            <thread>
                <tr>
                    <td>Id</td>
                    <td>Fullname</td>
                    <td>Email</td>
                    <td>Phone</td>
                    <td>Adress</td>
                    <td>Age</td>
                    <td>Birthday</td>
                    <td>Gender</td>
                    <td>Action</td>
                </tr>
            </thread>
            <tbody>
                <div class="error"><?php echo $this->session->flashdata('message-error');?></div>
                <div class="success"><?php echo $this->session->flashdata('message-success');?></div>
                <?php if (isset($listUser) && is_array($listUser) && count($listUser))?>
                <?php foreach($listUser as $key => $val) {?>
                <tr>
                    <td><?php echo $val['id'];?></td>
                    <td><?php echo $val['fullname'];?></td>
                    <td><?php echo $val['email'];?></td>
                    <td><?php echo $val['phone'];?></td>
                    <td><?php echo $val['address'];?></td>
                    <td><?php echo $val['age'];?></td>
                    <td><?php echo $val['birthday'];?></td>
                    <td><?php echo $val['gender'];?></td>
                    <td>
                        <a class="uk-button-danger" href="user/delete/<?php echo $val['id'];?>">Delete</a>
                        <a class="uk-button-success" href="user/update/<?php echo $val['id'];?>">Update</a>
                    </td>
                </tr>
            <?php }?>
            </tbody>
        </table>
        </div>
    </div>
</div>
</div>
</body>
</html>