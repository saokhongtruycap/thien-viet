<!DOCTYPE html>
<html>
<head>
    <link href="../../../resources/uikit/css/uikit.css" rel="stylesheet" />
    <link href="../../../resources/plugin.css" rel="stylesheet" />
    <link href="../../../resources/style.css" rel="stylesheet" />
    <base href="<?php echo BASE_URL;?>"
</head>
<body>
<div class="delete">
    <div class="uk-container uk-container-center">
        <h1>Danh sách người dùng</h1>
        <form action="user/delete" method="post">
            <div class="alert">Bạn có chắc là muốn xóa <?php echo $user['fullname'];?></div>
            <input type="hidden" value="<?php echo $user['id'];?>" name="id">
            <input type="submit" name="delete" value="Delete">
        </form>
    </div>
</div>
</div>
</body>
</html>