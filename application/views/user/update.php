<!DOCTYPE html>
<html>
<head>
    <link href="../../../resources/uikit/css/uikit.css" rel="stylesheet" />
    <link href="../../../resources/plugin.css" rel="stylesheet" />
    <link href="../../../resources/style.css" rel="stylesheet" />
    <base href="<?php echo BASE_URL;?>"
</head>
<body>
<?php $error = validation_errors(); echo (isset($error) && !empty($error)) ? $error : ''?>
<div class="create">
    <div class="uk-container uk-container-center">
        <div class="form">
            <h2>Update thành viên</h2>
            <form action="" method="post">
                <div>
                    <input type="text" value="<?php echo set_value('fullname', $user['fullname']);?>" name="fullname" placeholder="Fullname"/>
                </div>
                <div>
                    <input type="text" value="<?php echo set_value('email', $user['email']);?>" name="email" placeholder="Email"/>
                    <input type="hidden" value="<?php echo $user['email'];?>" name="email_original">
                </div>
                <div>
                    <input type="text" value="<?php echo set_value('phone', $user['phone']);?>" name="phone" placeholder="Phone"/>
                    <input type="hidden" value="<?php echo $user['phone'];?>" name="phone_original">
                </div>
                <div>
                    <input type="text" value="<?php echo set_value('address', $user['address']);?>" name="address" placeholder="Address"/>
                </div>
                <div>
                    <input type="text" value="<?php echo set_value('age', $user['age']);?>" name="age" placeholder="Age"/>
                </div>
                <div>
                    <input type="text" value="<?php echo set_value('birthday', $user['birthday']);?>" name="birthday" placeholder="Birthday"/>
                </div>
                <div>
                    <input type="radio" name="gender" value="male">Male
                    <input type="radio" name="gender" value="female">Female
                </div>
                <div>
                    <input type="submit" value="Update" name="update">
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</body>
</html>