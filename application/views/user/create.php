<!DOCTYPE html>
<html>
<head>
    <link href="../../../resources/uikit/css/uikit.css" rel="stylesheet" />
    <link href="../../../resources/plugin.css" rel="stylesheet" />
    <link href="../../../resources/style.css" rel="stylesheet" />
    <base href="<?php echo BASE_URL;?>"
</head>
<body>
<?php $error = validation_errors(); echo (isset($error) && !empty($error)) ? $error : ''?>
<div class="create">
    <div class="uk-container uk-container-center">
        <div class="form">
            <h2>Register</h2>
            <form action="user/create" method="post">
                <div>
                    <input type="text" value="<?php echo set_value('fullname');?>" name="fullname" placeholder="Fullname"/>
                </div>
                <div>
                    <input type="text" value="<?php echo set_value('email');?>" name="email" placeholder="Email"/>
                </div>
                <div>
                    <input type="password" value="" name="password" placeholder="Password"/>
                </div>
                <div>
                    <input type="text" value="<?php echo set_value('phone');?>" name="phone" placeholder="Phone"/>
                </div>
                <div>
                    <input type="text" value="<?php echo set_value('address');?>" name="address" placeholder="Address"/>
                </div>
                <div>
                    <input type="text" value="<?php echo set_value('age');?>" name="age" placeholder="Age"/>
                </div>
                <div>
                    <input type="text" value="<?php echo set_value('birthday');?>" name="birthday" placeholder="Birthday"/>
                </div>
                <div>
                    <input type="radio" name="gender" value="male">Male
                    <input type="radio" name="gender" value="female">Female
                </div>
                <div>
                    <input type="submit" value="Create" name="create">
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>