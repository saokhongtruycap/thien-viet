<header class="pc-header uk-visible-large">
<!--    <a href="" title=""><img src="../resources/img/slide1.jpg" alt=""></a>-->
    <div class="pc-header_top">
        <div class="uk-container uk-container-center">
            <div class="uk-flex uk-flex-space-between uk-flex-middle">
                <div class="logo">
                    <a href="" title=""><img src="../resources/img/companylogo.png" alt=""></a>
                </div>
                <div class="box-search">
                    <form action="" method="get" class="search-form">
                        <div class="uk-flex uk-flex-middle">
                            <div class="search">
                                <input type="text" name="search" value="" class="keyword" placeholder="Từ khóa cần tìm">
                            </div>
                            <button type="submit" name="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </form>
                </div>
                <div class="box-bussiness-hour">
                    <div class="uk-flex uk-flex-space-between uk-flex-middle">
                        <div class="item-bussiness_hour">
                            <div class="uk-flex uk-flex-space-between uk-flex-middle">
                                <div class="item-bussiness_hour__img">
                                    <a href="" title=""><img src="../resources/img/icon-clock.png" alt=""></a>
                                </div>
                                <div class="item-bussiness_hour__title">
                                    <h4>Giờ mở cửa</h4>
                                    <span>8H00 - 19H00</span>
                                </div>
                            </div>
                        </div>
                        <div class="item-bussiness_contact">
                            <div class="uk-flex uk-flex-space-between uk-flex-middle">
                                <div class="item-bussiness_contact__img">
                                    <a href="" title=""><img src="../resources/img/icon-mobile.png" alt=""></a>
                                </div>
                                <div class="item-bussiness_contact__title">
                                    <h4>Hotline</h4>
                                    <span>
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        <a href="" title="">1900 63 66 69</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="item-bussiness_lang">
                            <div class="uk-flex uk-flex-space-between uk-flex-middle">
                                <div class="item-bussiness_lang__eng">
                                    <a href="" title=""><img src="../resources/img/lang-en.png" alt=""></a>
                                </div>
                                <div class="item-bussiness_lang__vi">
                                    <a href="" title=""><img src="../resources/img/lang-vi.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- pc-header_top -->

    <div class="pc-header_menu">
        <div class="uk-container uk-container-center">
            <div class="uk-flex uk-flex-space-between uk-flex-middle">
                <div class="menu-categories">
                    <div class="menu-categories_title">
                        <a href="" title="">Danh mục sản phẩm</a>
                    </div>
                    <div class="menu-categories-hide">
                        <ul class="categories-hide uk-list uk-clearfix">
                            <li>
                                <?php
                                $header = array('SOFA', 'BÀN TRÀ', 'GHẾ BÀN TRÀ', 'KỆ TIVI', 'BÀN TRANG TRÍ', 'TỦ TIỀN SẢNH', 'TỦ ĐỂ GIẦY', 'ĐÔN HOA & ĐÔN TRANG TRÍ', 'TỦ BẢO QUẢN RƯỢU', 'TỦ RƯỢU', 'TỦ CIGAR', 'TINH DẦU KHUẾCH TÁN');
                                ?>
                                <a href="" title="" class="caret">Nội thất phòng khách</a>
                                <div class="fly-menu">
                                    <div class="uk-grid uk-grid-medium">
                                        <div class="uk-width-large-3-4">
                                            <div class="fly-menu_item">
                                                <ul class="uk-clearfix uk-list uk-grid uk-grid-medium uk-grid-width-large-1-4">
                                                    <?php for($i = 0; $i<count($header); $i++){ ?>

                                                    <li>
                                                        <div class="item">
                                                            <h4 class="item-title_header"><a href="" title=""><?php echo $header[$i]; ?></a></h4>
                                                            <a href="" title="" class="image img-scaledown img-zoomin"><img src="../resources/img/lvroom<?php echo $i+1;?>.jpg" alt=""></a>
                                                        </div>
                                                    </li>
                                                    <?php }	 ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="uk-width-large-1-4">
                                            <div class="fly-menu_banner">
                                                <img src="../resources/img/lvroombanner.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </li>
                            <li>
                                <?php
                                $header = array('BỘ SƯU TẬP PHÒNG NGỦ', 'GIƯỜNG', 'TỦ ĐẦU GIƯỜNG', 'GHẾ ĐUÔI GIƯỜNG', 'BÀN TRANG ĐIỂM', 'TỦ QUẦN ÁO', 'GA GỐI', 'ĐỆM', 'GƯƠNG ĐỨNG', 'CÂY TREO QUẦN ÁO');
                                ?>
                                <a href="" title="" class="caret">Nội thất phòng ngủ</a>
                                <div class="fly-menu">
                                    <div class="uk-grid uk-grid-medium">
                                        <div class="uk-width-large-3-4">
                                            <div class="fly-menu_item">
                                                <ul class="uk-clearfix uk-list uk-grid uk-grid-medium uk-grid-width-large-1-4">
                                                    <?php for($i = 0; $i<count($header); $i++){ ?>

                                                        <li>
                                                            <div class="item">
                                                                <h4 class="item-title_header"><a href="" title=""><?php echo $header[$i]; ?></a></h4>
                                                                <a href="" title="" class="image img-scaledown img-zoomin"><img src="../resources/img/bdroom<?php echo $i+1;?>.jpg" alt=""></a>
                                                            </div>
                                                        </li>
                                                    <?php }	 ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="uk-width-large-1-4">
                                            <div class="fly-menu_banner">
                                                <img src="../resources/img/lvroombanner.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <?php
                                $header = array('BỘ TỦ BẾP', 'TỦ LẠNH CAO CẤP', 'CHẬU RỬA BÁT');
                                ?>
                                <a href="" title="" class="caret">Nội thất phòng bếp</a>
                                <div class="fly-menu">
                                    <div class="uk-grid uk-grid-medium">
                                        <div class="uk-width-large-3-4">
                                            <div class="fly-menu_item">
                                                <ul class="uk-clearfix uk-list uk-grid uk-grid-medium uk-grid-width-large-1-4">
                                                    <?php for($i = 0; $i<count($header); $i++){ ?>

                                                        <li>
                                                            <div class="item">
                                                                <h4 class="item-title_header"><a href="" title=""><?php echo $header[$i]; ?></a></h4>
                                                                <a href="" title="" class="image img-scaledown img-zoomin"><img src="../resources/img/kitchen<?php echo $i+1;?>.jpg" alt=""></a>
                                                            </div>
                                                        </li>
                                                    <?php }	 ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="uk-width-large-1-4">
                                            <div class="fly-menu_banner">
                                                <img src="../resources/img/lvroombanner.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <?php
                                $header = array('BÀN ĂN', 'GHẾ BÀN ĂN', 'TỦ CHUẨN BỊ', 'BỘ ĐỒ ĂN');
                                ?>
                                <a href="" title="" class="caret">Nội thất phòng ăn</a>
                                <div class="fly-menu">
                                    <div class="uk-grid uk-grid-medium">
                                        <div class="uk-width-large-3-4">
                                            <div class="fly-menu_item">
                                                <ul class="uk-clearfix uk-list uk-grid uk-grid-medium uk-grid-width-large-1-4">
                                                    <?php for($i = 0; $i<count($header); $i++){ ?>

                                                        <li>
                                                            <div class="item">
                                                                <h4 class="item-title_header"><a href="" title=""><?php echo $header[$i]; ?></a></h4>
                                                                <a href="" title="" class="image img-scaledown img-zoomin"><img src="../resources/img/dining<?php echo $i+1;?>.jpg" alt=""></a>
                                                            </div>
                                                        </li>
                                                    <?php }	 ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="uk-width-large-1-4">
                                            <div class="fly-menu_banner">
                                                <img src="../resources/img/lvroombanner.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <?php
                                $header = array('BÀN LÀM VIỆC', 'GHẾ GIÁM ĐỐC', 'KỆ SÁCH & TỦ TÀI LIỆU');
                                ?>
                                <a href="" title="" class="caret">Nội thất văn phòng</a>
                                <div class="fly-menu">
                                    <div class="uk-grid uk-grid-medium">
                                        <div class="uk-width-large-3-4">
                                            <div class="fly-menu_item">
                                                <ul class="uk-clearfix uk-list uk-grid uk-grid-medium uk-grid-width-large-1-4">
                                                    <?php for($i = 0; $i<count($header); $i++){ ?>

                                                        <li>
                                                            <div class="item">
                                                                <h4 class="item-title_header"><a href="" title=""><?php echo $header[$i]; ?></a></h4>
                                                                <a href="" title="" class="image img-scaledown img-zoomin"><img src="../resources/img/office<?php echo $i+1;?>.jpg" alt=""></a>
                                                            </div>
                                                        </li>
                                                    <?php }	 ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="uk-width-large-1-4">
                                            <div class="fly-menu_banner">
                                                <img src="../resources/img/lvroombanner.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <?php
                                $header = array('BỒN TẮM', 'BỒN CẦU', 'CHẬU RỬA', 'BỘ TỦ CHẬU GƯƠNG', 'BỒN TIỂU', 'SEN', 'VÒI', 'PHỤ KIỆN PHÒNG TẮM');
                                ?>
                                <a href="" title="" class="caret">Nội thất phòng tắm</a>
                                <div class="fly-menu">
                                    <div class="uk-grid uk-grid-medium">
                                        <div class="uk-width-large-3-4">
                                            <div class="fly-menu_item">
                                                <ul class="uk-clearfix uk-list uk-grid uk-grid-medium uk-grid-width-large-1-4">
                                                    <?php for($i = 0; $i<count($header); $i++){ ?>

                                                        <li>
                                                            <div class="item">
                                                                <h4 class="item-title_header"><a href="" title=""><?php echo $header[$i]; ?></a></h4>
                                                                <a href="" title="" class="image img-scaledown img-zoomin"><img src="../resources/img/bathroom<?php echo $i+1;?>.jpg" alt=""></a>
                                                            </div>
                                                        </li>
                                                    <?php }	 ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="uk-width-large-1-4">
                                            <div class="fly-menu_banner">
                                                <img src="../resources/img/lvroombanner.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <?php
                                $header = array('VÂN ĐÁ MARBLE', 'VÂN ĐÁ STONE', 'VÂN GỖ', 'VÂN XI MĂNG', 'GIẢ CỔ', 'SÂN VƯỜN - TRANG TRÍ', 'GẠCH MÀU', 'PHỤ KIỆN ỐP LÁT GẠCH');
                                ?>
                                <a href="" title="" class="caret">gạch ốp lát</a>
                                <div class="fly-menu">
                                    <div class="uk-grid uk-grid-medium">
                                        <div class="uk-width-large-3-4">
                                            <div class="fly-menu_item">
                                                <ul class="uk-clearfix uk-list uk-grid uk-grid-medium uk-grid-width-large-1-4">
                                                    <?php for($i = 0; $i<count($header); $i++){ ?>

                                                        <li>
                                                            <div class="item">
                                                                <h4 class="item-title_header"><a href="" title=""><?php echo $header[$i]; ?></a></h4>
                                                                <a href="" title="" class="image img-scaledown img-zoomin"><img src="../resources/img/stone<?php echo $i+1;?>.jpg" alt=""></a>
                                                            </div>
                                                        </li>
                                                    <?php }	 ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="uk-width-large-1-4">
                                            <div class="fly-menu_banner">
                                                <img src="../resources/img/lvroombanner.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li><a href="" title="">đèn trang trí</a></li>
                            <li><a href="" title="">Bàn ghế ngoài trời</a></li>
                            <li><a href="" title="">đồng hồ</a></li>
                            <li><a href="" title="">khóa cửa</a></li>
                            <li><a href="" title="">công tắc</a></li>
                            <li><a href="" title="">ổ cắm điện</a></li>
                            <li><a href="" title="">thảm</a></li>
                            <li><a href="" title="">đồ trang trí</a></li>
                        </ul>
                    </div>
                </div>
                <div class="pc-header_main">
                    <div class="uk-flex uk-flex-space-between uk-flex-middle">
                        <nav class="sitelink">
                            <ul class="main-menu uk-list uk-clearfix">
                                <li><a href="" title="" class="active">Trang chủ</a></li>
                                <li><a href="" title="">về chúng tôi</a></li>
                                <li><a href="" title="">dự án</a></li>
                                <li><a href="" title="">đối tác</a></li>
                                <li>
                                    <a href="" title="" class="collection">bộ sưu tập</a>
                                    <div class="menu-collection">
                                        <ul class="sub-menu_collection uk-grid uk-grid-medium uk-grid-width-large-1-3">
                                            <li>
                                                <a href="" title="" class="active">Nội thất</a>
                                                <div class="sub-menu_collection__item">
                                                    <ul class="uk-list uk-clearfix">
                                                        <li><a href="" title="">Valencia</a></li>
                                                        <li><a href="" title="">Valentina</a></li>
                                                        <li><a href="" title="">Regina Patinata</a></li>
                                                        <li><a href="" title="">Vero</a></li>
                                                        <li><a href="" title="">Absolute</a></li>
                                                    </ul>
                                                    <div class="view-more">
                                                        <a href="" title="">Xem thêm</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <a href="" title="" class="active">Gạch ốp lát</a>
                                                <div class="sub-menu_collection__item">
                                                    <ul class="uk-list uk-clearfix">
                                                        <li><a href="" title="">Bộ gạch vân đá marble Travertino</a></li>
                                                        <li><a href="" title="">Bộ gạch vân gỗ Oxford</a></li>
                                                        <li><a href="" title="">Bộ gạch vân đá marble Varesse</a></li>
                                                        <li><a href="" title="">Bộ gạch màu Mosaico</a></li>
                                                        <li><a href="" title="">Bộ gạch màu Luxe</a></li>
                                                    </ul>
                                                    <div class="view-more">
                                                        <a href="" title="">Xem thêm</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <a href="" title="" class="active">Thiết bị vệ sinh</a>
                                                <div class="sub-menu_collection__item">
                                                    <ul class="uk-list uk-clearfix">
                                                        <li><a href="" title="">Bộ phụ kiện Axor montreux</a></li>
                                                        <li><a href="" title="">Bộ phụ kiện Eletech</a></li>
                                                        <li><a href="" title="">Bộ phụ kiện Nakar</a></li>
                                                        <li><a href="" title="">Bộ phụ kiện S1</a></li>
                                                        <li><a href="" title="">Bộ phụ kiện S3</a></li>
                                                    </ul>
                                                    <div class="view-more">
                                                        <a href="" title="">Xem thêm</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li><a href="" title="">đang khuyến mại</a></li>
                                <li>
                                    <a href="" title="" class="news">tin tức</a>
                                    <div class="menu-news">
                                        <ul class="sub-menu_news uk-list uk-clearfix">
                                            <li><a href="" title="">Cẩm nang kiến thức</a></li>
                                            <li><a href="" title="">Tin sản phẩm</a></li>
                                            <li><a href="" title="">Tin nội bộ</a></li>
                                            <li><a href="" title="">Tin khuyến mại</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li><a href="" title="">tuyển dụng</a></li>
                                <li><a href="" title="">liên hệ</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
