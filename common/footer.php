<footer class="footer">
    <section class="footer-partner">
        <div class="uk-container uk-container-center">
        <?php
                $owlInit = array(
                    'margin' => 30,
                    'lazyload' => true,
                    'rewindNav' => false,
                    'nav' => true,
                    'navText' => ['<i class="fa fa-arrow-left" aria-hidden="true"></i>','<i class="fa fa-arrow-right" aria-hidden="true"></i>'],
                    'autoplay' => false,
                    'smartSpeed' => 1000,
                    'autoplayTimeout' => 3000,
                    'dots' => false,
                    'loop' => false,
                    'responsive' => array(
                        0 => array(
                            'items' => 1.5,
                        ),
                        600 => array(
                            'items' => 2.5,
                        ),
                        1000 => array(
                            'items' => 6,
                        ),
                    )
                );
        ?>
            <div class="owl-slide">
                <div class="owl-carousel owl-theme" data-option="<?php echo base64_encode(json_encode($owlInit)); ?>">
                    <?php for ($i = 0; $i < 15; $i++) { ?>
                        <div class="wrap-item_logo">
                            <a class="image img-scaledown" href="" title=""><img src="../resources/img/logo<?php echo $i+1;?>.jpg" alt=""></a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section><!-- footer-partner -->

    <section class="footer-page">
        <div class="top-footer">
            <div class="uk-container uk-container-center">
                <div class="uk-grid uk-grid-medium">
                    <div class="uk-width-large-3-5">
                        <div class="top-footer_newsletter">
                            <div class="uk-grid uk-grid-medium">
                                <div class="uk-width-large-1-3">
                                    <div class="newsletter-header">
                                        <h4>Bản tin điện tử</h4>
                                    </div>
                                </div>
                                <div class="uk-width-large-2-3">
                                    <div class="newsletter-text">
                                        <div class="text">Đăng ký để cập nhật thông tin mới nhất về khuyến mãi, sản phẩm và sự kiện.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-large-2-5">
                        <div class="top-footer_form">
                            <form action="" method="">
                                <div class="top-footer_form__cover">
                                    <span class="form-icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" placeholder="Email">
                                    <button type="submit" class="form-submit"><a href="" title=""><i class="fa fa-angle-right" aria-hidden="true"></i></a></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- top-footer -->

    <section class="contact-footer">
        <div class="uk-container uk-container-center">
            <div class="contact-footer_cover">
                <div class="uk-grid uk-grid-medium uk-grid-width-large-1-5">
                    <div class="logo-footer">
                        <a class="image img-scaledown" href="" title=""><img src="../resources/img/footer2.png" alt=""></a>
                    </div>
                    <div class="list-menu-footer">
                        <div class="list-menu-footer__cover">
                            <h4>Hùng Túy</h4>
                            <ul class="menu uk-list uk-clearfix">
                                <li><a href="" title="">Về chúng tôi</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="list-menu-footer">
                        <div class="list-menu-footer__cover">
                            <h4>hỗ trợ khách hàng</h4>
                            <ul class="menu uk-list uk-clearfix">
                                <li><a href="" title="">Chương trình khuyến mại</a></li>
                                <li><a href="" title="">Tuyển dụng</a></li>
                                <li><a href="" title="">Tin tức sự kiện</a></li>
                                <li><a href="" title="">Cẩm nang kiến thức</a></li>
                                <li><a href="" title="">Diễn đàn</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="list-menu-footer">
                        <div class="list-menu-footer__cover">
                            <h4>hướng dẫn mua hàng</h4>
                            <ul class="menu uk-list uk-clearfix">
                                <li><a href="" title="">Chính sách giao hàng tận nhà</a></li>
                                <li><a href="" title="">Chính sách đổi trả hàng</a></li>
                                <li><a href="" title="">Chính sách mua hàng</a></li>
                                <li><a href="" title="">Chính sách bảo hành</a></li>
                                <li><a href="" title="">Chính sách bảo mật thông tin</a></li>
                                <li><a href="" title="">Quy định và hình thức thanh toán</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="list-menu-footer">
                        <div class="list-menu-footer__cover">
                            <h4>thông tin thêm</h4>
                            <ul class="menu uk-list uk-clearfix">
                                <li><a href="" title="">Sitemap</a></li>
                                <li><a href="" title="">Dự án</a></li>
                                <li><a href="" title="">Đối tác</a></li>
                                <li><a href="" title="">Diễn đàn</a></li>
                                <li><a href="" title="">Bộ sưu tập sản phẩm</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- contact-footer -->

    <section class="bottom-footer">
        <div class="uk-container uk-container-center">
            <div class="uk-grid uk-grid-medium uk-flex uk-flex-middle">
                <div class="uk-width-large-1-5">
                    <div class="bottom-footer_social">
                        <ul class="uk-flex uk-list uk-clearfix">
                            <li><a href="" title=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="" title=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            <li><a href="" title=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="uk-width-large-4-5">
                    <div class="bottom-footer_title">
                        <div class="bottom-footer_title__text">
                            <p>©2017 <b>SHOWROOM HÙNG TUÝ</b></p>
                            <p><b>Công ty TNHH Hoàng Tử</b>. Giấy CNĐKDN: 0100229851, đăng ký lần đầu ngày 06/12/1996, đăng ký thay đổi lần thứ 18 ngày 16/09/2016, cấp bởi Sở KHĐT thành phố Hà Nội.</p>
                            <p>Địa chỉ: 20 Cát Linh, Đống Đa, Hà Nội.</p>
                            <p>Hotline: 1900 63 66 69.  Email: info@hungtuy.com.vn</p>
                        </div>
                        <div class="bottom-footer_title__logo">
                            <a href="" title=""><img src="../resources/img/ftlogo.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</footer>
