<div class="breadcrumb">
    <div class="breadcrumb-img img-cover">
        <img src="../resources/img/breadcum.jpg" alt="" />
    </div>
	<div class="uk-container uk-container-center">
        <div class="breadcrumb-title">
            <div class="breadcrumb-title_header">
                <h4 class="title-header">Giới thiệu</h4>
            </div>
            <ul class="uk-breadcrumb uk-flex uk-flex-center">
                <li><a href="" title="">Trang Chủ</a></li>
                <li><a href="" title="">Giới Thiệu</a></li>
            </ul>
        </div>
	</div>
</div><!-- .breadcrumb -->